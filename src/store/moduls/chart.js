export default ({
    namespaced: true,
    state: {
        charts: []
    },
    mutations: {
        addChart(state, chart){
            state.charts.push(chart);
        },
    }
})